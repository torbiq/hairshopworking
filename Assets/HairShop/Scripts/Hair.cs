﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Hair : MonoBehaviour {

    private List<Transform> _curls;
    private List<Transform> _inactiveCurls;
    private List<Transform> _washedCurls;

    private enum Mode {
        MODE_CUTTING,

        MODE_SHAVING,
        MODE_GROWING,

        MODE_WASHING,
        MODE_DRYING,

        MODE_STRAIGHTENING,
        MODE_SCREWING,

        MODE_COLORING,
    }

    #region Public variables
    [SerializeField]
    private Mode _mode;
    [SerializeField]
    private Color _color;
    [SerializeField]
    private float _minLength;
    [SerializeField]
    private float _maxLength;
    [SerializeField]
    private float _growingSpeed;
    [SerializeField]
    private float _rotationSpeed;
    [SerializeField]
    private Sprite _stragihtCurlSprite,
                   _screwedCurlSprite;
    #endregion

    #region Buttons
    [SerializeField]
    private Button _buttonExit,
        _growButton,
        _cutButton,
        _paintButton,
        _shaveButton,
        _washButton,
        _straightButton,
        _screwButton,
        _dryButton;
    #endregion

    #region Shorthands
    private bool IsInRange(float resizedLength) {
        return _minLength <= resizedLength && resizedLength <= _maxLength;
    }
    private void Look2DAtMouse(Transform compaired) {
        Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - compaired.position;
        diff.Normalize();
        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        compaired.rotation = Quaternion.Lerp(compaired.rotation, Quaternion.Euler(0f, 0f, rot_z - 90), _rotationSpeed);
    }
    private void Look2DBackwardsToMouse(Transform compaired) {
        Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - compaired.position;
        diff.Normalize();
        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        compaired.rotation = Quaternion.Lerp(compaired.rotation, Quaternion.Euler(0f, 0f, rot_z - 90 - 180), _rotationSpeed);
    }
    private void SetInactiveCurlsActivationState(bool state) {
        foreach (var curl in _inactiveCurls) {
            curl.gameObject.SetActive(state);
        }
    }
    private void Deactivate(Collider2D hairTouched) {
        hairTouched.gameObject.SetActive(false);
        _inactiveCurls.Add(hairTouched.transform);
    }
    private void Activate(Collider2D hairTouched) {
        hairTouched.gameObject.SetActive(true);
        _inactiveCurls.Remove(hairTouched.transform);
    }
    #endregion

    #region Hair Actions
    private void Cut(Collider2D hairTouched) {
        throw new System.NotImplementedException();
    }
    private void Shave(Collider2D hairTouched) {
        if (hairTouched.gameObject.activeSelf) {
            Deactivate(hairTouched);
        }
        hairTouched.transform.localScale = Vector3.one * _minLength;
        Deactivate(hairTouched);
    }
    private void Grow(Collider2D hairTouched) {
        if (_inactiveCurls.Contains(hairTouched.transform)) {
            Activate(hairTouched);
        }
        float nextGrowingSize = Mathf.Lerp(hairTouched.transform.localScale.y, _maxLength, _growingSpeed);
        if (IsInRange(nextGrowingSize)) {
            hairTouched.transform.localScale = Vector3.one * nextGrowingSize;
        }
    }
    private void Wash(Collider2D hairTouched) {
        if (!_washedCurls.Contains(hairTouched.transform)) {
            _washedCurls.Add(hairTouched.transform);
        }
    }
    private void Dry(Collider2D hairTouched) {
        if (_washedCurls.Contains(hairTouched.transform)) {
            _washedCurls.Remove(hairTouched.transform);
        }
        Look2DBackwardsToMouse(hairTouched.transform);
    }
    private void Straight(Collider2D hairTouched) {
        Look2DAtMouse(hairTouched.transform);
        hairTouched.GetComponent<SpriteRenderer>().sprite = _stragihtCurlSprite;
    }
    private void Screw(Collider2D hairTouched) {
        Look2DAtMouse(hairTouched.transform);
        hairTouched.GetComponent<SpriteRenderer>().sprite = _screwedCurlSprite;
    }
    private void Color(Collider2D hairTouched) {
        var color = _color;
        color.a = 1;
        hairTouched.GetComponent<SpriteRenderer>().color = color;
    }
    #endregion

    private void Awake() {
        _curls = new List<Transform>();
        _inactiveCurls = new List<Transform>();
        _washedCurls = new List<Transform>();
        foreach (Transform child in transform) {
            _curls.Add(child);
            if (!child.gameObject.activeSelf) {
                _inactiveCurls.Add(child);
            }
        }
        _buttonExit.onClick.AddListener(Application.Quit);
        _growButton.onClick.AddListener(delegate { _mode = Mode.MODE_GROWING; });
        _cutButton.onClick.AddListener(delegate { _mode = Mode.MODE_CUTTING; });
        _paintButton.onClick.AddListener(delegate { _mode = Mode.MODE_COLORING; });
        _shaveButton.onClick.AddListener(delegate { _mode = Mode.MODE_SHAVING; });
        _washButton.onClick.AddListener(delegate { _mode = Mode.MODE_WASHING; });
        _straightButton.onClick.AddListener(delegate { _mode = Mode.MODE_STRAIGHTENING; });
        _screwButton.onClick.AddListener(delegate { _mode = Mode.MODE_SCREWING; });
        _dryButton.onClick.AddListener(delegate { _mode = Mode.MODE_DRYING; });
    }

    void Update() {
        foreach (var curl in _curls) {
            Quaternion rotation = curl.transform.rotation;
            rotation.eulerAngles += new Vector3(0, 0, Mathf.Sin(Time.time * 2.5f) * 0.25f);
            curl.transform.rotation = rotation;
        }
        foreach (var curl in _washedCurls) {
            curl.transform.rotation = Quaternion.Lerp(curl.transform.rotation, Quaternion.Euler(new Vector3(0, 0, -180)), _rotationSpeed  );
        }
        if (Input.GetMouseButton(0)) {
            Collider2D[] collidersTouched;
            SetInactiveCurlsActivationState(true);
            switch (_mode) {
                case Mode.MODE_CUTTING:
                    collidersTouched = Physics2D.OverlapCircleAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), 0.25f);
                    foreach (var collider in collidersTouched) {
                        //Cut(collider);
                    }
                    break;
                case Mode.MODE_SHAVING:
                    collidersTouched = Physics2D.OverlapCircleAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), 0.25f);
                    foreach (var collider in collidersTouched) {
                        Shave(collider);
                    }
                    break;
                case Mode.MODE_GROWING:
                    collidersTouched = Physics2D.OverlapCircleAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), 0.25f);
                    foreach (var collider in collidersTouched) {
                        Grow(collider);
                    }
                    break;
                case Mode.MODE_WASHING:
                    collidersTouched = Physics2D.OverlapCircleAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), 0.25f);
                    foreach (var collider in collidersTouched) {
                        Wash(collider);
                    }
                    break;
                case Mode.MODE_DRYING:
                    collidersTouched = Physics2D.OverlapCircleAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), 15f);
                    foreach (var collider in collidersTouched) {
                        Dry(collider);
                    }
                    break;
                case Mode.MODE_STRAIGHTENING:
                    collidersTouched = Physics2D.OverlapCircleAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), 1f);
                    foreach (var collider in collidersTouched) {
                        Straight(collider);
                    }
                    break;
                case Mode.MODE_SCREWING:
                    collidersTouched = Physics2D.OverlapCircleAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), 1f);
                    foreach (var collider in collidersTouched) {
                        Screw(collider);
                    }
                    break;
                case Mode.MODE_COLORING:
                    collidersTouched = Physics2D.OverlapCircleAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), 0.25f);
                    foreach (var collider in collidersTouched) {
                        Color(collider);
                    }
                    break;
                default:
                    break;
            }
            SetInactiveCurlsActivationState(false);
        }
    }
}
