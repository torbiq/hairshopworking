﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.Events;

//public class CollisionEvent2DController : MonoBehaviour {
//    public Connection belongTo { get; set; }

//    public delegate void OnCollision2DEvent(Collision2D collision2d);
//    public delegate void OnCollider2DEvent(Collider2D collider2d);

//    public event OnCollision2DEvent OnCollisionEnter2DEvent;
//    public event OnCollision2DEvent OnCollisionStay2DEvent;
//    public event OnCollision2DEvent OnCollisionExit2DEvent;

//    public event OnCollider2DEvent OnTriggerEnter2DEvent;
//    public event OnCollider2DEvent OnTriggerStay2DEvent;
//    public event OnCollider2DEvent OnTriggerExit2DEvent;
    
//    private void OnTriggerEnter2D(Collider2D collider2d) {
//        if (OnTriggerEnter2DEvent != null) {
//            OnTriggerEnter2DEvent(collider2d);
//        }
//    }

//    private void OnTriggerStay2D(Collider2D collider2d) {
//        if (OnTriggerStay2DEvent != null) {
//            OnTriggerStay2DEvent(collider2d);
//        }
//    }

//    private void OnTriggerExit2D(Collider2D collider2d) {
//        if (OnTriggerExit2DEvent != null) {
//            OnTriggerExit2DEvent(collider2d);
//        }
//    }

//    private void OnCollisionEnter2D(Collision2D collision2d) {
//        if (OnCollisionEnter2DEvent != null) {
//            OnCollisionEnter2DEvent(collision2d);
//        }
//    }

//    private void OnCollisionStay2D(Collision2D collision2d) {
//        if (OnCollisionStay2DEvent != null) {
//            OnCollisionStay2DEvent(collision2d);
//        }
//    }

//    private void OnCollisionExit2D(Collision2D collision2d) {
//        if (OnCollisionExit2DEvent != null) {
//            OnCollisionExit2DEvent(collision2d);
//        }
//    }
//}
